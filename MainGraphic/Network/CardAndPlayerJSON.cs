﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uno.Network
{
    class CardAndPlayerJSON
    {
        public Player activePlayer { get; set; }
        public List<Card> playedCards { get; set; }
    }

    class Player
    {
        public bool newPlayer { get; set; }
        public int NbrOfNextPlayer { get; set; }
    }

    class Card
    {
        public int varible { get; set; }
        public int color { get; set; }
        public int CardType { get; set; }
    }


}
