﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace Uno.Network
{
    class Receiver
    {
        ///
        ///Receives data from server
        ///Data that should be recieved:
        ///Deck info, next card, number of cards on hand for each player, played cards, and other game info
        ///Maybe chatlog receives?

        private TcpListener listener;
        private Socket messages;
        private bool connected;
        public bool updated;
        private List<string> allMessages;
        private Object safety;
        
        public Receiver()
        {
            safety = new Object();
            connected = false;
            updated = false;
            allMessages = new List<string>();
        }

        //called as thread! Should be better later on
        public void receiveMessages()
        {
            while (!connected)
            {}
            while (true)
            {
                string message = "";
                byte[] data = new byte[100];
                try
                {
                    int size = messages.Receive(data);
                    for (int i = 0; i < size; i++)
                    {
                        message += Convert.ToChar(data[i]);
                    }
                    if (message.Length != 0)
                    {
                        lock (safety)
                        {
                            allMessages.Add(message);
                            updated = true;
                        }

                    }
                }
                catch (Exception e) { }
            }
        }

        public string getFirstMessageInQueue()
        {
            int count = 0;
            lock (safety)
            {
                count = allMessages.Count;
            }
            if(count != 0)
            {
                string message = "";
                lock (safety)
                {
                    message = allMessages.First();
                    allMessages.Remove(message);
                    count = allMessages.Count();
                    if (count == 0) updated = false;
                }
                return message;
            }
            else
            {
                throw new InvalidProgramException("No queue");
            }
        }

        
        public void connectToServer(string serverAddress, int port)
        {
            IPAddress ip;
            if (IPAddress.TryParse(serverAddress, out ip))
            {
                listener = new TcpListener(ip, port);
                listener.Start();
                messages = listener.AcceptSocket();
                connected = true;
            }
        }
    }
}
