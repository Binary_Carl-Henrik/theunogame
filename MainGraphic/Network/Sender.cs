﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Uno.Network
{
    class Sender
    {
        ///
        ///Sends data to server(host)
        ///data that should be sent is:
        ///Number of cards on hand, which card/cards that are played
        ///Chatlog? Send messages to server?
        ///
        private TcpClient server;
        private List<string> buffer;
        private Object bufferLock;

        public Sender()
        {
            server = new TcpClient();
            buffer = new List<string>();
            bufferLock = new Object();
            //BUGG!!! Stänger ej av sig vid avstängning av programet
            Thread sending = new Thread(new ThreadStart(messageSender));
            sending.Start();
        }
        //Arbetande
        public void sendMessage(string message)
        {
            string bufferMessage = message;
            //handle as json?

            //steps before this
            lock (bufferLock)
            {
                buffer.Add(bufferMessage);
            }
        }

        public void connectToServer(string serverAddress, int port)
        {
            //Redigera så att servern kan gå ner kort och att servern kan starta efter alla andra. Kanske med hjälp av att starta receiver och server före?
            server.Connect(serverAddress, port);
        }


        private void messageSender()
        {
            //Should be adapted;
            
            while (true)
            {
                bool empty = true;
                lock (bufferLock)
                {
                    empty = buffer.Count == 0;
                }
                if (!empty && server.Connected)
                {
                    Stream messageStream = server.GetStream();
                    ASCIIEncoding messageEncoding = new ASCIIEncoding();
                    string message = "";
                    lock (bufferLock)
                    {
                        message = buffer.First();
                        buffer.Remove(message);
                    }
                    byte[] encodedMessage = messageEncoding.GetBytes(message);
                    messageStream.Write(encodedMessage, 0, encodedMessage.Length);
                }
            }
        }
        
    }
}
