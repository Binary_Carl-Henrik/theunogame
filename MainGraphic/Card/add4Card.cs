﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uno.Card
{
    class Add4Card : ICards
    {
        public static readonly int CARDTYPE = 4;
        public int CardType
        {
            get
            {
                return CARDTYPE;
            }
        }

        public int Color { private set; get; }
        public int Number { private set; get; }

        public Add4Card()
        {
            Color = -1;
            Number = 4;
        }

        public int getCardType()
        {
            return CARDTYPE;
        }

        public void setColor(int color)
        {
            Color = color;
        }

        public int isAcceptable(ICards card)
        {
            if(card.Color == Color || card.CardType == CARDTYPE)
            {
                return 0;
            }
            return -1;
        }
    }
}
