﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uno.Card
{
    interface ICards
    {
        int Color { get; }
        int Number { get; }
        int CardType { get; }
        /// <summary>
        /// 0 = NumberCard
        /// </summary>
        /// <returns></returns>
        int getCardType();
        int isAcceptable(ICards card);
    }
}
