﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uno.Card
{
    class SwitchColorCard : ICards
    {
        public static readonly int CARDTYPE = 1;
        public int CardType { private set; get; }

        public int Color { private set; get; }
        public int Number { private set; get; }

        public int getCardType()
        {
            return CARDTYPE;
        }
        public int isAcceptable(ICards card)
        {
            throw new NotImplementedException();
        }
    }
}
