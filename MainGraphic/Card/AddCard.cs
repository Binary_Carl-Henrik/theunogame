﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uno.Card
{
    class AddCard : ICards
    {
        public static readonly int CARDTYPE = 3;
        public int CardType
        {
            get
            {
                return CARDTYPE;
            }
        }
        public int Color { private set; get; }
        public int Number { private set; get; }

        public int getCardType()
        {
            return CARDTYPE;
        }
        public int isAcceptable(ICards card)
        {
            throw new NotImplementedException();
        }
    }
}
