﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uno.Card
{
    class NumberCard : ICards
    {
        public static readonly int CARDTYPE = 0;
        public int CardType
        {
            get
            {
                return CARDTYPE;
            }
        }

        public int Color { private set; get; }
        public int Number { private set; get; }
        public NumberCard(int color, int number)
        {
            Color = color;
            Number = number;
        }

        public int getCardType()
        {
            return CARDTYPE;
        }
        /// <summary>
        /// Checks if the card is acceptable
        /// </summary>
        /// <param name="card"></param>
        /// <returns>1 if next person are blocked next turn, 0 if acceptable, -1 if not acceptable</returns>
        public int isAcceptable(ICards card)
        {
            if (card.getCardType() == Add4Card.CARDTYPE || card.getCardType() == SwitchColorCard.CARDTYPE)
            {
                return 0;
            }
            if (card.getCardType() == NumberCard.CARDTYPE)
            {
                if (((NumberCard)card).Color == Color)
                {
                    return 0;
                }
                else if (((NumberCard)card).Number == Number)
                {
                    return 0;
                }
            }

            return -1;
        }
    }
}
