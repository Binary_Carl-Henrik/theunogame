﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uno.Card;
using Uno.Network;

namespace Uno.Engine
{
    class Table
    {
        private List<Player> players;
        private int nbrOfPlayers, activePlayer, nextPlayer;
        private int activeColor;
        public List<ICards> activeCard { private set; get; }
        private bool reversed, playerHavePlayed;
        private CardDeck deck;
        private Sender sender;
        private Receiver receiver;

        public Table(int PlayerAmount)
        {
            nbrOfPlayers = PlayerAmount;
            activePlayer = 0;
            activeColor = -1;
            nextPlayer = 1;
            reversed = false;
            players = new List<Player>();
            deck = new CardDeck();
            sender = new Sender();
            sender.connectToServer("192.168.1.87", 8001);
            receiver = new Receiver();

            for (int i = 0; i < nbrOfPlayers; i++)
            {
                createPlayer();
            }
            System.Diagnostics.Debug.WriteLine("Spelare 0: " + players.ElementAt(1).cardsOnHand.First().Number +  " nbr of players " + players.Count);
            playFirstCard();
        }

        public void givePlayerNewCard()
        {
            players.ElementAt(activePlayer).AddCards(deck.getCard());
        }

        public int playerCanPlay()
        {
            if (playerHavePlayed)
            {
                playerHavePlayed = false;
                return 0;
            }
            if (activeCard.Last().getCardType() == NumberCard.CARDTYPE)
            {
                return 1;
            }
            else if (activeCard.Last().getCardType() == BlockCard.CARDTYPE)
            {
                activeCard.Remove(activeCard.Last());
                return 0;
            }
            else if (activeCard.Last().getCardType() == AddCard.CARDTYPE)
            {
                int nbr = 0;
                foreach (ICards card in activeCard)
                {
                    if (card.getCardType() == AddCard.CARDTYPE)
                    {
                        nbr -= 2;
                    }
                }
                activeCard.Clear();
                return nbr;
            }
            else if (activeCard.Last().getCardType() == Add4Card.CARDTYPE)
            {
                int nbr = 0;
                foreach (ICards card in activeCard)
                {
                    if (card.getCardType() == Add4Card.CARDTYPE)
                    {
                        nbr -= 4;
                    }
                }
                activeCard.Clear();
                return nbr;
            }
            return 1;
        }

        public Player getNextPlayer()
        {
            //what?
            /*if (activeCard.Last().getCardType() == NumberCard.CARDTYPE)
            {
                ICards card = activeCard.Last();
                activeCard.Clear();
                activeCard.Add(card);
            }
            */
            System.Diagnostics.Debug.WriteLine(nextPlayer);
            System.Diagnostics.Debug.WriteLine(getNbrOfNextPlayer());
            Player player = players.ElementAt(nextPlayer);
            
            activePlayer = nextPlayer;
            nextPlayer = getNbrOfNextPlayer();
            return player;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cards"></param>
        /// <returns>-1 is cant play card, 0 can play, 1 needs to insert next color</returns>
        public bool playCard(List<ICards> cards)
        {
            if (cardsCanBePlayed(cards) != -1)
            {
                foreach (ICards card in cards)
                {
                    activeCard.Add(card);
                }
                players.ElementAt(activePlayer).removeCards(cards);
                if(cards.First().CardType == Add4Card.CARDTYPE)
                {
                    ((Add4Card)activeCard.Last()).setColor(activeColor);
                    activeColor = -1;
                }
                playerHavePlayed = true;
                sendCardsAndNextPlayer(cards, getNbrOfNextPlayer());
                return true;
            }
            return false;
        }

        public int cardsCanBePlayed(List<ICards> cards)
        {
            if (cards.Count == 0)
            {
                System.Diagnostics.Debug.WriteLine("No cards");
                return -1;
            }
            int cardType = cards.First().CardType;

            if (cardType == NumberCard.CARDTYPE)
            {
                System.Diagnostics.Debug.WriteLine("returns numbercard");
                return playNumberCards(cards);
            }
            else if (cardType == Add4Card.CARDTYPE)
            {
                return playAdd4Cards(cards);
            }
            return -1;
        }

        public void insertNextColor(int color)
        {
            activeColor = color;
        }

        private int playNumberCards(List<ICards> cards)
        {
            int nbr = cards.First().Number;
            System.Diagnostics.Debug.WriteLine("first card found");
            foreach (ICards card in cards)
            {
                if(card.getCardType() != NumberCard.CARDTYPE || card.Number != nbr)
                {
                    System.Diagnostics.Debug.WriteLine("wrong number");
                    return -1;
                }
            }
            if(activeCard.Last().isAcceptable(cards.First()) == 0)
            {
                return 0;
            }
            System.Diagnostics.Debug.WriteLine("First active: " + activeCard.Last().Number + " Color: " + activeCard.Last().Color + " first: " + cards.First().Number + " Color: " + cards.First().Color);
            System.Diagnostics.Debug.WriteLine("card first and last and compatible");
            return -1;
        }
        
        private int playAdd4Cards(List<ICards> cards)
        {
            foreach(ICards card in cards)
            {
                if(card.getCardType() != Add4Card.CARDTYPE)
                {
                    return -1;
                }
            }
            if(activeColor != -1)
            {
                return 0;
            }
            return 1;
        }

        private int getNbrOfNextPlayer()
        {
            int NextPlayerToPlay = 0;
            if (reversed)
            {
                NextPlayerToPlay = nextPlayer-1;
                if (NextPlayerToPlay < 0)
                {
                    NextPlayerToPlay = players.Count - 1;
                }
            }
            else
            {
                NextPlayerToPlay = nextPlayer+1;
                if (NextPlayerToPlay == players.Count)
                {
                    NextPlayerToPlay = 0;
                }
            }
            return NextPlayerToPlay;
        }

        private void playFirstCard()
        {
            ICards card = deck.getCard();
            while (card.getCardType() != NumberCard.CARDTYPE)
            {
                card = deck.getCard();
            }
            activeCard = new List<ICards>();
            activeCard.Add(card);
        }

        private void createPlayer()
        {
            Player player = new Player();
            for (int i = 0; i < 7; i++)
            {
                player.AddCards(deck.getCard());
            }
            System.Diagnostics.Debug.WriteLine("spalre");
            players.Add(player);
        }

        private void sendCardsAndNextPlayer(List<ICards> cards, int player)
        {
            string dataOfPlayedHand = JsonConvert.SerializeObject(new Network.CardAndPlayerJSON() { activePlayer = getPlayerJson(player), playedCards = getPlayedCardJson(cards) });
            sender.sendMessage(dataOfPlayedHand);
        }

        private Network.Player getPlayerJson(int player)
        {
            Network.Player nextPlayerToPlay = new Network.Player();
            nextPlayerToPlay.newPlayer = player == activePlayer;
            nextPlayerToPlay.NbrOfNextPlayer = player;
            return nextPlayerToPlay;
        }

        private List<Network.Card> getPlayedCardJson(List<ICards> cards)
        {
            List<Network.Card> playedCards = new List<Network.Card>();
            foreach(ICards card in cards)
            {
                playedCards.Add(new Network.Card() { color = card.Color, CardType = card.CardType, varible = card.Number });
            }
            return playedCards;
        }
    }
}
