﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uno.Card;

namespace Uno.Engine
{
    class CardDeck
    {
        private List<ICards> cards;

        public CardDeck()
        {
            cards = new List<ICards>();
            addNumberCards();
            add4Cards();
        }

        public ICards getCard()
        {
            Random rand = new Random();
            int cardNbr = rand.Next() % cards.Count;
            ICards card = cards.ElementAt(cardNbr);
            cards.Remove(card);
            return card;
        }

        private void addNumberCards()
        {
            for (int i = 1; i < 10; i++)
            {
                for (int c = 0; c < 4; c++)
                {
                    if (i == 0)
                    {
                        cards.Add(new NumberCard(c, i));
                    }
                    else {
                        cards.Add(new NumberCard(c, i));
                        cards.Add(new NumberCard(c, i));
                    }
                }
            }
        }

        private void add4Cards()
        {
            for(int i = 0; i<4; i++)
            {
                cards.Add(new Add4Card());
            }
        }
    }
}
