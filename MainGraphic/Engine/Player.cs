﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uno.Card;

namespace Uno.Engine
{
    class Player
    {
        public List<ICards> cardsOnHand { private set; get; }
        public Player()
        {
            cardsOnHand = new List<ICards>();
        }

        public bool removeCards(List<ICards> cards)
        {
            foreach(ICards card in cards)
            {
                cardsOnHand.Remove(card);
            }
            return true;
        }

        public void AddCards(ICards card)
        {
            cardsOnHand.Add(card);
        }

        public bool nextHandIsLast()
        {
            if (cardsOnHand.Count == 1 && cardsOnHand.First().getCardType() == NumberCard.CARDTYPE)
            {
                return true;
            }
            if ((cardsOnHand.First().getCardType() == NumberCard.CARDTYPE) && allCardsAreSameNumber())
            {
                return true;
            }
            return false;
        }

        private bool allCardsAreSameNumber()
        {
            if (cardsOnHand.First().getCardType() != NumberCard.CARDTYPE)
            {
                return false;
            }

            int nbr = cardsOnHand.First().Number;

            foreach (ICards card in cardsOnHand)
            {
                if (cardsOnHand.First().Number != nbr)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
