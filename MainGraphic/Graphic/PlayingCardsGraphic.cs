﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uno.Card;

namespace Uno.Graphic
{
    class PlayingCardsGraphic : ICardGraphic
    {
        public Texture2D Card { private set; get; }
        public Rectangle Position { private set; get; }

        public ICards CardInfo { private set; get; }

        public bool selected { private set; get; }

        public PlayingCardsGraphic(Texture2D card, Rectangle position, ICards cardInfo)
        {
            Card = card;
            Position = position;
            selected = false;
            CardInfo = cardInfo;
        }

        public void setCard(Texture2D card)
        {
            this.Card = card;
        }

        public void setPosition(Rectangle position)
        {
            Position = position;
        }

        public void addToPositionCoordinats(int x, int y)
        {
            int positionX = Position.X + x;
            int positionY = Position.Y + y;
            Position = new Rectangle(positionX, positionY, Position.Width, Position.Height);
        }

        public bool isInsidePosition(int x, int y)
        {
            if (Position.X <= x && Position.Width + Position.X >= x)
            {
                if (Position.Y <= y && Position.Height + Position.Y >= y)
                {
                    return true;
                }
            }
            return false;
        }

        public bool cardIsInPositionOfOtherCard(ICardGraphic card)
        {
            if (card.Position.X + 5 > Position.X && card.Position.X - 5 < Position.X)
            {
                if (card.Position.Y + 5 > Position.Y && card.Position.Y - 5 < Position.Y)
                {
                    return true;
                }
            }
            return false;
        }

        public void moveCardsToPositionOfCard(ICardGraphic card)
        {
            int newX = getNewCoordinat(card.Position.X, Position.X);
            int newY = getNewCoordinat(card.Position.Y, Position.Y);
            int newWidth = getNewSize(card.Position.Width, Position.Width);
            int newHeight = getNewSize(card.Position.Height, Position.Height);

            Position = new Rectangle(newX, newY, newWidth, newHeight);
        }

        private int getNewCoordinat(int coordinatTo, int oldCoordinat)
        {
            if (coordinatTo > oldCoordinat)
            {
                return oldCoordinat + 1 + (int)((coordinatTo - oldCoordinat) * 0.1);
            }
            else if (coordinatTo < oldCoordinat)
            {
                return oldCoordinat - (1 + (int)((oldCoordinat - coordinatTo) * 0.1));
            }
            else
            {
                return oldCoordinat;
            }
        }

        private int getNewSize(int sizeTo, int sizeFrom)
        {
            if (sizeTo > sizeFrom)
            {
                return sizeFrom + 1 + (int)((sizeTo - sizeFrom) * 0.1);
            }
            else if (sizeTo < sizeFrom)
            {
                return sizeFrom - (1 + (int)((sizeFrom - sizeTo) * 0.1));
            }
            else
            {
                return sizeFrom;
            }
        }

        public bool selectCard()
        {
            if (!selected)
            {
                Position = new Rectangle(Position.X, Position.Y - Position.Height, Position.Width, Position.Height);
            }
            else
            {
                Position = new Rectangle(Position.X, Position.Y + Position.Height, Position.Width, Position.Height);
            }
            selected = !selected;
            return selected;
        }

    }
}
