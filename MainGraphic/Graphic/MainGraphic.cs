﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Uno.Card;
using Uno.Engine;

namespace Uno.Graphic
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class MainGraphic : Game
    {
        public const int RED = 0, BLUE = 1, YELLOW = 2, GREEN = 3;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D table, MenuBar;
        PlayingCardsGraphic topCard;
        private CardTexture cardTextures;
        private List<IButton> buttons;
        private List<PlayingCardsGraphic> ActiveCards;
        private List<ICards> cardsToPlay;
        private List<List<Texture2D>> Opponents;
        private List<int> _cardSelectedOrder;
        private bool _animationActive;
        private SpriteFont font;

        //Temporary
        private Player player;
        private Table gameTable;
        private bool nextTurn;

        int TotalHeight, TotalWidth, GameScreenHeight, GameScreenWidth;

        MouseState previousMouse;
        KeyboardState previousKey;


        public MainGraphic()
        {
            graphics = new GraphicsDeviceManager(this);
            this.graphics.PreferredBackBufferHeight = 950;
            this.graphics.PreferredBackBufferWidth = 1800;
            Content.RootDirectory = "Content";
            ActiveCards = new List<PlayingCardsGraphic>();
            cardsToPlay = new List<ICards>();
            buttons = new List<IButton>();
            IsMouseVisible = true;
            previousMouse = Mouse.GetState();
            previousKey = Keyboard.GetState();
            _cardSelectedOrder = new List<int>();
            _animationActive = false;
        }

        protected override void OnActivated(object sender, EventArgs args)
        {
            Window.Title = "Activated app";
            base.OnActivated(sender, args);
        }

        protected override void OnDeactivated(object sender, EventArgs args)
        {
            Window.Title = "Deactive app";
            base.OnDeactivated(sender, args);
        }

        protected override void Initialize()
        {
            gameTable = new Table(2);
            nextTurn = true;
            base.Initialize();
        }

        //Split method into several methods
        protected override void LoadContent()
        {
            font = Content.Load<SpriteFont>("Text");
            spriteBatch = new SpriteBatch(GraphicsDevice);
            initializeCardTextures();
            
            TotalHeight = graphics.GraphicsDevice.Viewport.Height;
            TotalWidth = graphics.GraphicsDevice.Viewport.Width;
            GameScreenHeight = (int)(TotalHeight * 0.9);
            GameScreenWidth = TotalWidth;

            table = this.Content.Load<Texture2D>("Table");
            MenuBar = this.Content.Load<Texture2D>("MenuBar");
            
            //temp
            if(gameTable.activeCard[gameTable.activeCard.Count-1].CardType == NumberCard.CARDTYPE)
            {
                NumberCard card = (NumberCard)gameTable.activeCard[gameTable.activeCard.Count - 1];
                //should be DeckCardGraphic later on
                topCard = new PlayingCardsGraphic(cardTextures.getCardTexture(card.Color, card.Number), new Rectangle(GameScreenWidth / 2, GameScreenHeight / 2, 75, 100), card);
            }
            //

            addCards(cardTextures.getCardTexture(0, 1), new NumberCard(0, 1));
            addPlayButton();
            addDeckButton();

            updateSizeOfCards();
        }

        private void addPlayButton()
        {
            int width = TotalWidth / 15;
            int height = (int)((TotalHeight - GameScreenHeight) * 0.8);
            int x = TotalWidth - width - (int)(TotalWidth * 0.05);
            int y = (int)(GameScreenHeight + (TotalHeight - GameScreenHeight) * 0.1);
            buttons.Add(new Button(Content.Load<Texture2D>("PlayCardButton"), Content.Load<Texture2D>("PlayCardButton_Hovered"), new Rectangle(x, y, width, height)));
        }

        private void addDeckButton()
        {
            
            int width = TotalWidth / 22;
            int height = (int)(GameScreenHeight / 8);
            int x = TotalWidth / 2 - topCard.Position.Width - width/2 ;
            int y = GameScreenHeight / 2;
            buttons.Add(new DeckButton(Content.Load<Texture2D>("Backside"), Content.Load<Texture2D>("PlayCardButton_Hovered"), new Rectangle(x, y, width, height), "Test", Content.Load<SpriteFont>("Text"), Color.Black));
        }

        private void initializeCardTextures()
        {
            cardTextures = new CardTexture();
            for(int c = 0; c<4; c++)
            {
                for(int i = 1; i<10; i++)
                {
                    switch (c)
                    {
                        case RED:
                            cardTextures.addCard(Content.Load<Texture2D>("Red" + i.ToString()), c);
                            break;
                        case BLUE:
                            cardTextures.addCard(Content.Load<Texture2D>("Blue" + i.ToString()), c);
                            break;
                        case YELLOW:
                            cardTextures.addCard(Content.Load<Texture2D>("Yellow" + i.ToString()), c);
                            break;
                        case GREEN:
                            cardTextures.addCard(Content.Load<Texture2D>("Green" + i.ToString()), c);
                            break;
                    }
                    
                }
            }
            cardTextures.addCard(Content.Load<Texture2D>("add4card"), -1);
        }

        private void addCards(Texture2D card, ICards cardInfo)
        {
            ActiveCards.Add(new PlayingCardsGraphic(card, new Rectangle(), cardInfo));
        }

        private void updateSizeOfCards()
        {
            for (int i = 0; i < ActiveCards.Count; i++)
            {
                int x, y, width, height, space;
                width = GameScreenWidth / (12);
                if (ActiveCards.Count > 6)
                {
                    width = GameScreenWidth / (2 * ActiveCards.Count);
                }
                space = width / 5;
                height = GameScreenHeight / (5 * (1 + ActiveCards.Count / 8));
                x = ((GameScreenWidth / 2) + (width + space) * i) - (width + space) * ActiveCards.Count / 2;
                y = GameScreenHeight - height - 50;
                ActiveCards[i].setPosition(new Rectangle(x, y, width, height));
            }
        }

        protected override void UnloadContent()
        {
        }

        //split method into several methods
        protected override void Update(GameTime gameTime)
        {
            if (gameTable.playerCanPlay() != 1 || nextTurn)
            {
                player = gameTable.getNextPlayer();
                loadPlayer();
                nextTurn = false;
            }

            MouseState mouseState = Mouse.GetState();
            KeyboardState keyboardState = Keyboard.GetState();
            
            pressedCard(mouseState);
            buttonPressed(mouseState);

            //TEMP
            if (keyboardState.IsKeyDown(Keys.Space) && !previousKey.IsKeyDown(Keys.Space))
            {
                gameTable.givePlayerNewCard();
                loadPlayer();
            }
            //

            
            if (_animationActive)
            {
                if(animateCardMove() == 1)
                {
                    loadColorChoiceButtons();
                }
            }

            previousMouse = mouseState;
            previousKey = keyboardState;


            base.Update(gameTime);
        }

        private void pressedCard(MouseState mouseState)
        {
            if (mouseState.LeftButton == ButtonState.Pressed && previousMouse.LeftButton != mouseState.LeftButton)
            {
                try
                {
                    int i = getPressedCard(mouseState);
                    if (ActiveCards[i].selectCard())
                    {
                        _cardSelectedOrder.Add(i);
                    }
                    else
                    {
                        _cardSelectedOrder.Remove(i);
                    }
                }
                catch (IndexOutOfRangeException e)
                { }

            }
        }

        private void buttonPressed(MouseState mouseState)
        {
            foreach (IButton button in buttons)
            {
                if (button.isHovering(mouseState))
                {
                    if (mouseState.LeftButton == ButtonState.Pressed && previousMouse.LeftButton != mouseState.LeftButton)
                    {
                        if (button.ButtonType == Button.BUTTONTYPE)
                        {
                            _animationActive = true;
                        }
                        else if (button.ButtonType == ColorButton.BUTTONTYPE)
                        {
                            gameTable.insertNextColor(((ColorButton)button).Color);
                            _animationActive = true;
                            removeColorButtons();
                        }else if(button.ButtonType == DeckButton.BUTTONTYPE)
                        {
                            gameTable.givePlayerNewCard();
                            loadPlayer();
                        }
                    }
                }
            }
        }

        private void removeColorButtons()
        {
            List<IButton> newButtonList = new List<IButton>();
            foreach(Button button in buttons)
            {
                if(button.ButtonType != ColorButton.BUTTONTYPE)
                {
                    newButtonList.Add(button);
                }
            }
            buttons = newButtonList;
        }

        private void loadColorChoiceButtons()
        {
            int width = TotalWidth / 15;
            int height = (int)((TotalHeight - GameScreenHeight) * 0.5);
            int x = TotalWidth - width - (int)(TotalWidth * 0.05);
            int y = 0 + (int)(TotalHeight * 0.05);
            buttons.Add(new ColorButton(Content.Load<Texture2D>("RedButton"), Content.Load<Texture2D>("RedButton_Hover"), new Rectangle(x, y, width, height), RED));
            y += height + (int)(TotalHeight * 0.05);
            buttons.Add(new ColorButton(Content.Load<Texture2D>("BlueButton"), Content.Load<Texture2D>("BlueButton_Hover"), new Rectangle(x, y, width, height), BLUE));
            y += height + (int)(TotalHeight * 0.05);
            buttons.Add(new ColorButton(Content.Load<Texture2D>("YellowButton"), Content.Load<Texture2D>("YellowButton_Hover"), new Rectangle(x, y, width, height), YELLOW));
            y += height + (int)(TotalHeight * 0.05);
            buttons.Add(new ColorButton(Content.Load<Texture2D>("GreenButton"), Content.Load<Texture2D>("GreenButton_Hover"), new Rectangle(x, y, width, height),  GREEN));
        }

        private void loadPlayer()
        {
            ActiveCards.Clear();
            foreach(ICards card in player.cardsOnHand)
            {
                ActiveCards.Add(new PlayingCardsGraphic(cardTextures.getCardTexture(card.Color, card.Number), new Rectangle(), card));
            }
            updateSizeOfCards();
        }

        private void loadCompetingPlayers()
        {

        }

        private void moveCard()
        {
            List<PlayingCardsGraphic> restOfHand = new List<PlayingCardsGraphic>();
            foreach (PlayingCardsGraphic card in ActiveCards)
            {
                if (!card.selected)
                {
                    restOfHand.Add(card);
                }
            }
            if (gameTable.playCard(cardsToPlay))
            {
                cardsToPlay.Clear();
                ActiveCards = restOfHand;
                updateSizeOfCards();
            }
            
        }

        private int animateCardMove()
        {
            if(cardsToPlay.Count != 0)
            {
                if (!currentCardIsInPositionAndMoveForward())
                {
                    ActiveCards[_cardSelectedOrder[0]].moveCardsToPositionOfCard(topCard);
                }
                return 0;
            }
            else
            {
                int cardEvent = cardsCanBePlayed();
                if (cardEvent != 0)
                {
                    _animationActive = false;
                    cardsToPlay.Clear();
                }
                return cardEvent;
            }
            
        }

        private bool currentCardIsInPositionAndMoveForward()
        {
            if (ActiveCards[_cardSelectedOrder[0]].cardIsInPositionOfOtherCard(topCard))
            {
                topCard.setCard(ActiveCards[_cardSelectedOrder[0]].Card);
                _cardSelectedOrder.Remove(_cardSelectedOrder[0]);

                if (_cardSelectedOrder.Count == 0)
                {
                    moveCard();
                }
                return true;
            }
            return false;
        }

        private int cardsCanBePlayed()
        {
            foreach (int cardIndex in _cardSelectedOrder)
            {
                cardsToPlay.Add(ActiveCards[cardIndex].CardInfo);
            }
            switch (gameTable.cardsCanBePlayed(cardsToPlay))
            {
                case 0:
                    return 0;
                case 1:
                    //should return extra command
                    return 1;
                default:
                    _animationActive = false;
                    return -1;
            }
        }

        private int getPressedCard(MouseState state)
        {
            for (int i = 0; i < ActiveCards.Count; i++)
            {
                if (ActiveCards[i].isInsidePosition(state.X, state.Y))
                {
                    return i;
                }
            }
            throw new IndexOutOfRangeException();
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            var fps = 1 / gameTime.ElapsedGameTime.TotalSeconds;
            Window.Title = fps.ToString();

            spriteBatch.Begin();
            spriteBatch.Draw(table, destinationRectangle: new Rectangle(0, 0, GameScreenWidth, GameScreenHeight));
            spriteBatch.Draw(MenuBar, destinationRectangle: new Rectangle(0, GameScreenHeight, TotalWidth, TotalHeight - GameScreenHeight));

            foreach(IButton button in buttons)
            {
                spriteBatch.Draw(button.getButtonImage(), destinationRectangle: button.Position);
                if(button.ButtonType == DeckButton.BUTTONTYPE)
                {
                    spriteBatch.DrawString(((DeckButton)button).Font, ((DeckButton)button).nbrOfCardsLeft, ((DeckButton)button).TextPosition, Color.Black);
                }
            }

            for (int i = 0; i < ActiveCards.Count; i++)
            {
                spriteBatch.Draw(ActiveCards[i].Card, destinationRectangle: ActiveCards[i].Position);
            }
            spriteBatch.Draw(topCard.Card, destinationRectangle: topCard.Position);
            spriteBatch.DrawString(font, "Text", new Vector2(100, 100), Color.Black);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
