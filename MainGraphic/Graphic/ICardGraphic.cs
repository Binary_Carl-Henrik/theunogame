﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uno.Graphic
{
    interface ICardGraphic
    {
        Texture2D Card { get; }
        Rectangle Position { get; }
        bool selected { get; }

        void setCard(Texture2D card);

        void setPosition(Rectangle position);

        void addToPositionCoordinats(int x, int y);

        bool isInsidePosition(int x, int y);

        bool cardIsInPositionOfOtherCard(ICardGraphic card);

        void moveCardsToPositionOfCard(ICardGraphic card);

        bool selectCard();
    }
}
