﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uno.Graphic
{
    interface IButton
    {
        Rectangle Position { get; }
        int ButtonType { get; }


        bool isHovering(MouseState state);
        Texture2D getButtonImage();
    }


}
