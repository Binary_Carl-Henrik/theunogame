﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uno.Graphic
{
    class ColorButton:IButton
    {
        public static readonly int BUTTONTYPE = 1;
        private Texture2D _buttonImage, _hoveringImage;
        public Rectangle Position { private set; get; }
        private bool _hovering;
        public int ButtonType { private set; get; }
        public int Color { private set; get; }

        public ColorButton(Texture2D buttonimage, Texture2D hoveringimage, Rectangle position, int color)
        {
            _buttonImage = buttonimage;
            _hoveringImage = hoveringimage;
            Position = position;
            _hovering = false;
            ButtonType = BUTTONTYPE;
            Color = color;
        }

        public bool isHovering(MouseState state)
        {
            if (state.X >= Position.X && state.X <= Position.X + Position.Width)
            {
                if (state.Y >= Position.Y && state.Y <= Position.Y + Position.Height)
                {
                    _hovering = true;
                    return true;
                }
            }
            _hovering = false;
            return false;
        }

        public Texture2D getButtonImage()
        {
            if (_hovering)
            {
                return _hoveringImage;
            }
            return _buttonImage;
        }
    }
}
