﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uno.Graphic
{
    class CardTexture
    {
        GraphicsDeviceManager graphics;
        private List<Texture2D> _redCards;
        private List<Texture2D> _blueCards;
        private List<Texture2D> _yellowCards;
        private List<Texture2D> _greenCards;
        private List<Texture2D> _otherCards;
        public CardTexture()
        {
            _redCards = new List<Texture2D>();
            _blueCards = new List<Texture2D>();
            _yellowCards = new List<Texture2D>();
            _greenCards = new List<Texture2D>();
            _otherCards = new List<Texture2D>();
        }

        public Texture2D getCardTexture(int type, int number)
        {
            if(number>9 || number < 1 )
            {
                throw new ArgumentException("No acceptable number");
            }
            if(type == 0)
            {
                return _redCards.ElementAt(number-1);
            }else if(type == 1)
            {
                return _blueCards.ElementAt(number-1);
            }
            else if(type == 2)
            {
                return _yellowCards.ElementAt(number-1);
            }
            else if(type == 3)
            {
                return _greenCards.ElementAt(number-1);
            }else if(type == -1)
            {
                return _otherCards.ElementAt(0);
            }
            else
            {
                throw new ArgumentException("Not implemented");
            }
        }

        public void addCard(Texture2D texture, int color)
        {
            switch (color)
            {
                case 0:
                    _redCards.Add(texture);
                    break;
                case 1:
                    _blueCards.Add(texture);
                    break;
                case 2:
                    _yellowCards.Add(texture);
                    break;
                case 3:
                    _greenCards.Add(texture);
                    break;
                default:
                    _otherCards.Add(texture);
                    break;
            }
        }
    }
}
