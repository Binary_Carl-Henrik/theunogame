﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace Uno.Graphic
{
    class DeckButton : IButton
    {
        public static readonly int BUTTONTYPE = 2;
        public Rectangle Position { private set; get; }
        public Vector2 TextPosition { private set; get; }
        public SpriteFont Font { private set; get; }
        public int ButtonType { private set; get; }
        public String nbrOfCardsLeft { private set; get; }
        public Color TextColor { private set; get; }
        private int nbrOfCardsInDeck;
        private bool _hovering;
        private Texture2D _buttonImage, _hoveringImage;

        public DeckButton(Texture2D buttonimage, Texture2D hoveringimage, Rectangle position, String text, SpriteFont font, Color color)
        {
            _buttonImage = buttonimage;
            _hoveringImage = hoveringimage;
            Position = position;
            _hovering = false;
            ButtonType = BUTTONTYPE;
            nbrOfCardsInDeck = 0;
            Font = font;
            int TextLeft = (Position.Left + Position.Width) - (Position.Width / 2);
            int TextTop = (Position.Top + Position.Height) - (Position.Height);
            TextPosition = new Vector2(TextLeft, TextTop);
            nbrOfCardsLeft = nbrOfCardsInDeck.ToString() + "st";
            TextColor = color;
        }

        public void setNbrOfCardsInDeck(int nbr)
        {
            if(nbr >= 0)
            {
                nbrOfCardsInDeck = nbr;
                nbrOfCardsLeft = nbrOfCardsInDeck.ToString() + "st";
            }
        }

        public Texture2D getButtonImage()
        {
            if (_hovering)
            {
                return _hoveringImage;
            }
            return _buttonImage;
        }

        public bool isHovering(MouseState state)
        {
            if (state.X >= Position.X && state.X <= Position.X + Position.Width)
            {
                if (state.Y >= Position.Y && state.Y <= Position.Y + Position.Height)
                {
                    _hovering = true;
                    return true;
                }
            }
            _hovering = false;
            return false;
        }
        
    }
}
